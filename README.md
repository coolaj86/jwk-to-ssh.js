# JWK to SSH (for node.js)

A minimal library to encode a JWK
as an SSH public key (`id_rsa.pub`).

Works for RSA and ECDSA public keys.

Features
========

&lt; 75 lines of code | &lt; 0.7kb gzipped | 1.5kb minified | 2.1kb with comments

* [x] SSH Public Keys
* [x] OpenSSH Private Keys
* [x] RSA Keys
* [x] EC Keys
  * P-256 (prime256v1, secp256r1)
  * P-384 (secp384r1)
* [x] Browser Version
  * [Bluecrypt JWK to SSH](https://git.coolaj86.com/coolaj86/bluecrypt-jwk-to-ssh.js)

Note: the file size stats are for v1.0 which did not include private key packing.
I plan to go back and update the stats, but just know that it grew a little over 2x.

### Need JWK to SSH? PEM to SSH?

Try one of these:

* [jwk-to-ssh.js](https://git.coolaj86.com/coolaj86/jwk-to-ssh.js) (RSA + EC)
* [Eckles.js](https://git.coolaj86.com/coolaj86/eckles.js) (more EC utils)
* [Rasha.js](https://git.coolaj86.com/coolaj86/eckles.js) (more RSA utils)

### Need Alternate SSH Private Keys?

This library supports OpenSSH private keys.

  * [x] OpenSSH
  * [ ] Normal PKCS1 / SEC1 / PKCS8
    * [x] Rasha.js
    * [x] Eckles.js
  * [ ] Putty

# Library Usage

You can also use it from JavaScript:

```js
var fs = require('fs');
var jwktossh = require('jwk-to-ssh');

var jwk = JSON.parse(fs.readFileSync("./privkey.jwk.json"));
var pub = jwktossh.pack({
  jwk: jwk
, comment: 'root@localhost'
, public: true
});

console.info(pub);
```

# CLI Usage

You can install `jwk-to-ssh` and use it from command line:

```bash
npm install -g jwk-to-ssh
```

```bash
jwk-to-ssh [keyfile] [comment] [public]
```

```bash
jwk-to-ssh pubkey.jwk.json
```

```bash
jwk-to-ssh privkey.jwk.json root@localhost
```

```bash
jwk-to-ssh privkey.jwk.json root@localhost public
```

Legal
-----

[jwk-to-ssh.js](https://git.coolaj86.com/coolaj86/jwk-to-ssh.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)
