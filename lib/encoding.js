'use strict';

var Enc = module.exports;

Enc.base64ToHex = function (b64) {
  return Buffer.from(b64, 'base64').toString('hex');
};

Enc.binToBuf = function (bin) {
  return Buffer.from(bin, 'binary');
};

Enc.bufToBase64 = function (u8) {
  return Buffer.from(u8).toString('base64');
};

Enc.binToHex = function (bin) {
  return Buffer.from(bin, 'binary').toString('hex');
};

Enc.hexToBase64 = function (hex) {
  return Buffer.from(hex, 'hex').toString('base64');
};

Enc.hexToBin = function (hex) {
  return Buffer.from(hex, 'hex').toString('binary');
};
