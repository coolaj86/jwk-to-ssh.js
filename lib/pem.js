'use strict';

var PEM = module.exports;
var Enc = require('./encoding.js');

PEM.packBlock = function (opts) {
  return '-----BEGIN ' + opts.type + '-----\n'
    + Enc.bufToBase64(opts.bytes).match(/.{1,70}/g).join('\n') + '\n'
    + '-----END ' + opts.type + '-----'
  ;
};
