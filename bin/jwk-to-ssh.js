#!/usr/bin/env node
'use strict';

var path = require('path');
var jwktossh = require('../index.js');

var pubfile = process.argv[2];
var comment = process.argv[3] || 'root@localhost';
var pub = ('public' === process.argv[4]);

if (!pubfile) {
  console.error("specify a path to JWK");
  process.exit(1);
}

var jwk = require(path.join(process.cwd(), pubfile));
var out = jwktossh.pack({ jwk: jwk, comment: comment, public: pub });

console.info(out);
