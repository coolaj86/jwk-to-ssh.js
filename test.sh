#!/bin/bash
set -e

chmod 0600 fixtures/*

# creates a new key
#ssh-keygen -t rsa -b 2048 -C root@localhost -N '' -f fixtures/privkey-rsa-2048.openssh.pem.3
node bin/jwk-to-ssh.js fixtures/privkey-rsa-2048.jwk.json root@localhost > fixtures/privkey-rsa-2048.openssh.pem
node bin/jwk-to-ssh.js fixtures/privkey-rsa-2048.jwk.json root@localhost public > fixtures/pub-rsa-2048.ssh.pub

node bin/jwk-to-ssh.js fixtures/privkey-ec-p256.jwk.json root@localhost > fixtures/privkey-ec-p256.openssh.pem
node bin/jwk-to-ssh.js fixtures/privkey-ec-p256.jwk.json root@localhost public > fixtures/pub-ec-p256.ssh.pub

node bin/jwk-to-ssh.js fixtures/privkey-ec-p384.jwk.json root@localhost > fixtures/privkey-ec-p384.openssh.pem
node bin/jwk-to-ssh.js fixtures/privkey-ec-p384.jwk.json root@localhost public > fixtures/pub-ec-p384.ssh.pub

# changes embedded comment and creates new random dummy checksum
ssh-keygen -c -C root@localhost -f fixtures/privkey-rsa-2048.openssh.pem
ssh-keygen -c -C root@localhost -f fixtures/privkey-ec-p256.openssh.pem
ssh-keygen -c -C root@localhost -f fixtures/privkey-ec-p384.openssh.pem

# convert to public key
ssh-keygen -y -C root@localhost -f fixtures/privkey-rsa-2048.openssh.pem
ssh-keygen -y -C root@localhost -f fixtures/privkey-ec-p256.openssh.pem
ssh-keygen -y -C root@localhost -f fixtures/privkey-ec-p384.openssh.pem

diff fixtures/pub-rsa-2048.ssh.pub fixtures/privkey-rsa-2048.openssh.pem.pub
diff fixtures/pub-ec-p256.ssh.pub fixtures/privkey-ec-p256.openssh.pem.pub
diff fixtures/pub-ec-p384.ssh.pub fixtures/privkey-ec-p384.openssh.pem.pub

#ssh-keygen -e -m PKCS8 -f fixtures/privkey-ec-p256.openssh.pem
#ssh-keygen -e -m RFC4716 -f fixtures/privkey-ec-p256.openssh.pem
